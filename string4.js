function titleCase(word){
    word = word.toLowerCase().trim();
    if(word.length >= 1){
        word = word[0].toUpperCase() + word.substring(1);
        return word + ' ';
    }
    else{
        return '';
    }
}

function fullName(string){
    if(typeof(string) !== 'object'){
        return;
    }
    let name = '';
    if(string.first_name !== undefined){
        if(typeof(string.first_name) !== 'string'){
            return;
        }
        name += titleCase(string.first_name); 
    }
    if(string.middle_name !== undefined){
        if(typeof(string.middle_name) !== 'string'){
            return;
        }
        name += titleCase(string.middle_name);
    }
    if(string.last_name !== undefined){
        if(typeof(string.last_name) !== 'string'){
            return;
        }
        name += titleCase(string.last_name);
    }
    return name.trimEnd();
}

module.exports = { fullName };
