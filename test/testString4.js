const string4 = require('../string4');

let obj = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};
let fname = string4.fullName(obj);
console.log(fname);

obj = {"first_name": "JoHN", "last_name": "SMith"};
fname = string4.fullName(obj);
console.log(fname);

obj = {"first_name": "   ", "middle_name": "    doe    "};
fname = string4.fullName(obj);
console.log(fname);

obj = {"middle_name": "doe"};
fname = string4.fullName(obj);
console.log(fname);

obj = {"first_name": 23, "middle_name": "doe"};
fname = string4.fullName(obj);
console.log(fname);

fname = string4.fullName({});
console.log(fname);

fname = string4.fullName('hello world');
console.log(fname);